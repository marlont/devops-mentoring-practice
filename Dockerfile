# BUILDING
FROM node:16.20.0 as build
# Declaring workdir for 'build' image
WORKDIR /app
# Copying package.json to workdir to could install dependencies
COPY package.json .
# Intalling production dependencies, (running npm install --omit=dev doesn't work in Dockerfile)
RUN npm run install:prod
# Copying all the repo files except for the mentioned in .dockerignore
COPY . .
# Building app
RUN npm run build

# RUNNING
FROM nginx
# Coyping from 'build' image the generated build to be pasted on the nginx server
COPY --from=build /app/build /usr/share/nginx/html
# Exposing port 80 for run production app by http protocol
EXPOSE 80
