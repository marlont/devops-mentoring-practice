import React from 'react';
import Quiz from './components/Quiz';
import './App.css'

function App() {
  return (
    <div className="app">
      <h1 className='quiz-title'>Quiz BTS</h1>
      <h2 className='quiz-subtitle'>Devops for Software Engineers</h2>
      <img className='App-logo' src="logo512.png" alt="BTS logo" />
      <Quiz />
    </div>
  );
}

export default App;
