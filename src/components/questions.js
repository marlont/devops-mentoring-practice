const questions = [
    {
      question: "¿Qué es Docker?",
      answers: [
        "Una plataforma para desarrollar aplicaciones web.",
        "Un sistema operativo virtualizado.",
        "Una herramienta para la automatización de despliegues.",
        "Una tecnología de contenedores."
      ],
      correctAnswer: 3
    },
    {
      question: "¿Cuál es el propósito principal de Docker?",
      answers: [
        "Crear máquinas virtuales.",
        "Gestionar el ciclo de vida del software.",
        "Facilitar el desarrollo de aplicaciones distribuidas.",
        "Garantizar la seguridad de los contenedores."
      ],
      correctAnswer: 2
    },
    {
      question: "¿Qué es Docker-compose?",
      answers: [
        "Una herramienta para la gestión de repositorios de código fuente.",
        "Una solución para el despliegue de aplicaciones en la nube.",
        "Un sistema de orquestación de contenedores.",
        "Una plataforma para el desarrollo de aplicaciones móviles."
      ],
      correctAnswer: 2
    },
    {
      question: "¿Cuál de las siguientes opciones es un componente fundamental de Docker?",
      answers: [
        "Dockerfile",
        "Docker Engine",
        "Docker Compose",
        "Docker Swarm"
      ],
      correctAnswer: 1
    },
    {
      question: "¿Cuál es el comando para crear una imagen de Docker a partir de un Dockerfile?",
      answers: [
        "docker run",
        "docker build",
        "docker create",
        "docker image"
      ],
      correctAnswer: 1
    },
    {
      question: "¿Qué es GitLab?",
      answers: [
        "Una plataforma de gestión de bases de datos.",
        "Un sistema operativo de código abierto.",
        "Una herramienta de control de versiones.",
        "Una plataforma de desarrollo colaborativo."
      ],
      correctAnswer: 3
    },
    {
      question: "¿Cuál de las siguientes opciones es una característica de GitLab?",
      answers: [
        "Control de versiones distribuido.",
        "Gestión de tareas y proyectos.",
        "Integración continua y entrega continua (CI/CD).",
        "Todos los anteriores."
      ],
      correctAnswer: 3
    },
    {
      question: "¿Qué es un repositorio en GitLab?",
      answers: [
        "Un servidor que almacena imágenes de Docker.",
        "Un directorio local donde se guarda el código fuente.",
        "Un sistema de seguimiento de errores y problemas.",
        "Un espacio en GitLab donde se guarda y gestiona el código fuente."
      ],
      correctAnswer: 3
    },
    {
      question: "¿Qué es un servicio en Docker-compose?",
      answers: [
        "Una instancia de un contenedor Docker en ejecución.",
        "Un componente de Docker que administra los recursos del sistema.",
        "Una herramienta para la orquestación de múltiples contenedores.",
        "Un archivo de configuración para la construcción de imágenes de Docker."
      ],
      correctAnswer: 0
    },
    {
      question: "¿Cuál de las siguientes opciones se utiliza para definir la infraestructura de una aplicación en Docker-compose?",
      answers: [
        "Dockerfile",
        "docker-compose.yml",
        "docker-compose build",
        "Docker Hub"
      ],
      correctAnswer: 1
    }
  ];
  
  export default questions;
  